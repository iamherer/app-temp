
#include "pch.h"
#include "App.h"

int main()
{

    OglApp::Desc config;
    config.width = 800;
    config.height = 600;
    config.posX = 200;
    config.posY = 200;

    strcpy(config.title, "tools Panel");

    Line::App app(config);

    if (!app.Init())
    {
        LOGE("cannot create app");
        return 1;
    }
    app.Execute();
    app.OnDestroy();

    return 0;
};
