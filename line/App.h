#pragma once

#include "pch.h"
#include "imgui/EditApp.h"

using BaseApp = EditApp;

namespace Line
{

    struct App : public BaseApp
    {
        App(const Desc &desc);
        bool Init() override;
        void OnFrameRender() override;
        void OnEdit() override;

    };
};