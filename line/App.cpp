#include "App.h"
#include "gut/Mesh.h"

Line::App::App(const Desc &desc)
    : BaseApp(desc){};

bool Line::App::Init()
{
    X_FAILED(BaseApp::Init());

    return true;
}

void Line::App::OnFrameRender()
{

    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glClear(GL_DEPTH_BUFFER_BIT);

    glClear(GL_COLOR_BUFFER_BIT);
    glClearColor(1.0f, 0.5f, 1.0f, 1.0f);

    glClear(GL_STENCIL_BUFFER_BIT);
}

void Line::App::OnEdit()
{

    if (ImGui::Begin("hello world"))
    {
        ImGui::Text("hello world");
    }

    ImGui::End();
}
